package nl.hetisetenstijd.androidapp;

import com.auth0.android.jwt.JWT;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Class responsible for holding user data.
 * @author Tim Medema
 */
public class User {

    private static final JSONParser PARSER = new JSONParser();
    private static User instance;

    /**
     * Getting the instance of the user.
     * @return The singleton instance.
     */
    public static User getInstance(){
        if(instance == null)
            instance = new User();

        return instance;
    }

    private JWT token;

    /**
     * Constructor, doesn't do anything, just making it private.
     */
    private User(){}

    /**
     * Sets the token. Token is expected in JSON format. Token is parsed out of the JSON and turned into a JWT object.
     * @param token Token in the format: {"token":"[token here]"}
     * @throws ParseException Thrown when the JSON provided is invalid.
     */
    public void setToken(String token) throws ParseException {
        JSONObject obj = (JSONObject) PARSER.parse(token); //Parse the JSON
        String actualToken = (String) obj.get("token"); assert actualToken != null; //Grab value for key token, and check if it exists.
        JWT jwt = new JWT(actualToken); //Create new object.

        this.token = jwt; //Save it
    }

    /**
     * Gets the first name of the user.
     * @return The first name of the user.
     */
    public String getFirstName(){
        return token.getClaim("first_name").asString(); //Grab the "first_name" claim from the token as String, and return it.
    }

    /**
     * Gets the ID of the user.
     * @return The ID of the USER
     */
    public int getUserId() {
        return token.getClaim("id").asInt(); //Grab the "id" claim from the token as String, and return it.
    }
}
