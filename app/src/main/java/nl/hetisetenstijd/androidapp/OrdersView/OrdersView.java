package nl.hetisetenstijd.androidapp.OrdersView;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nl.hetisetenstijd.androidapp.API;
import nl.hetisetenstijd.androidapp.R;
import nl.hetisetenstijd.androidapp.User;

/**
 * Controller for the order view screen.
 */
public class OrdersView extends Fragment {

    private static JSONParser PARSER = new JSONParser();
    private TextView orderList;

    /**
     * Gets called when the view gets created.
     * @return The view
     */
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        View view = inflater.inflate(R.layout.orders_screen, container, false);
        orderList = view.findViewById(R.id.orders_list);
        return view;
    }

    /**
     * Gets called when the view has been created.
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onViewCreated(@NonNull final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState); //call super

        view.findViewById(R.id.refresh_button).setOnClickListener(new View.OnClickListener() { //Find refresh button and register on click
            @Override
            public void onClick(View v) {
                setOrders(); //call setOrders function
            }
        });

        String greetingMsg = getResources().getString(R.string.welcome_orders); //Get string
        String greetingFormat = String.format(greetingMsg, User.getInstance().getFirstName()); //Format
        TextView greeting = view.findViewById(R.id.greeting_orders); //Get greeting_orders text view
        greeting.setText(greetingFormat); //set text

        setOrders(); //Set the orders
    }

    public void setOrders(){
        Runnable runnable = new Runnable() { //Create new runnable to run on a different thread, because no networking is allowed on the main thread
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                List<Order> orders = new ArrayList<>(); //Create new list of orders
                JSONArray array = null; //New JSONArray object
                try {
                    array = (JSONArray) PARSER.parse(API.getInstance().getOrders()); //Parse the orders from the API
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                assert array != null; //Check if array exists
                for(int i = (array.size() - 1); i >= 0; i--){ //loop over orders in reverse order
                    JSONObject innerObj = (JSONObject) array.get(i); //get the object
                    orders.add(new Order((long)innerObj.get("id"), (long)innerObj.get("isDelivered"), (String)innerObj.get("created"))); //create new Order and and to list
                }

                StringBuilder sb = new StringBuilder(); //Create new StringBuilder

                for(Order order : orders){ //loop over the orders
                    sb.append(order.getId()).append(": ").append(order.getTimePlaced()).append("\n"); //Append the order ID and time placed
                    sb.append("Status: ").append(order.isDelivered() ? "Bezorgd" : "Onderweg").append("\n\n"); //On a new line append the status
                }

                orderList.setText(sb.toString().trim()); //Set the text from the StringBuilder
            }
        };

        new Thread(runnable).start(); //Create new thread and start it.
    }

}