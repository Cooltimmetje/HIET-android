package nl.hetisetenstijd.androidapp.OrdersView;

/**
 * This class represents a order and holds all information that the app needs to display it.
 * @author Tim Medema
 */
public class Order {

    private long id;
    private boolean isDelivered;
    private String timePlaced;

    /**
     * Constructor
     * @param id The id of the order
     * @param isDelivered Whether the order has been delivered
     * @param timePlaced The time the order has been placed.
     */
    public Order(long id, boolean isDelivered, String timePlaced){
        this.id = id;
        this.isDelivered = isDelivered;
        this.timePlaced = timePlaced;
    }

    /**
     * Constructor
     * @param id The id of the order
     * @param isDelivered Numeric value of whether the order has been delivered (0 = false, 1 = true)
     * @param timePlaced The time the order has been placed.
     */
    public Order(long id, long isDelivered, String timePlaced) {
        this.id = id;
        this.isDelivered = isDelivered == 1;
        this.timePlaced = timePlaced;
    }

    /**
     * Gets the id of the order
     * @return The id of the order
     */
    public long getId() {
        return id;
    }

    /**
     * Gets if the order has been delivered
     * @return if the order has been delivered.
     */
    public boolean isDelivered() {
        return isDelivered;
    }

    /**
     * Gets the time when the order has been placed.
     * @return the time when the order has been placed.
     */
    public String getTimePlaced() {
        return timePlaced;
    }
}
