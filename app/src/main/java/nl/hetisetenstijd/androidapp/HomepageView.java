package nl.hetisetenstijd.androidapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

/**
 * Homepage controller
 */
public class HomepageView extends Fragment {

    /**
     * Gets called when the view gets created.
     * @return The view
     */
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.homepage, container, false);
    }

    /**
     * Gets called when the view has been created.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState); //call super

        String companyName = getResources().getString(R.string.company_name); //Get company_name string.
        String greetingFormat = getResources().getString(R.string.no_login_greeting); //Get no_login_greeting string.
        String greetingMsg = String.format(greetingFormat, companyName); //Format string
        TextView greeting = view.findViewById(R.id.greeting); //get the TextView greeting
        greeting.setText(greetingMsg); //Set text

        view.findViewById(R.id.go_login_button).setOnClickListener(new View.OnClickListener() { //find go_login_button and register onclick
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(HomepageView.this).navigate(R.id.go_to_login); //Go to the login screen.
            }
        });
    }
}