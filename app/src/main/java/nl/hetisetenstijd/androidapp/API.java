package nl.hetisetenstijd.androidapp;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * API class handling api calls
 * @author Tim Medema
 */
public class API {

    private static final String URL = "http://uwot.skuddbot.xyz";
    private static final int PORT = 5000;

    private static API instance;

    /**
     * Getting the instance of the API.
     * @return The singleton instance.
     */
    public static API getInstance(){
        if(instance == null)
            instance = new API();

        return instance;
    }

    /**
     * Logs the user in
     * @param email The email of the user.
     * @param password The password of the user
     * @return The token in a JSON, or a error.
     * @throws IOException When the connection has failed
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String login(String email, String password) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault(); //Create new ClosableHttpClient
        HttpPost post = new HttpPost(URL + ":" + PORT + "/users/login"); //Create new Post request

        List<NameValuePair> params = new ArrayList<>(2); //List for params
        params.add(new BasicNameValuePair("email", email)); //Add email param
        params.add(new BasicNameValuePair("password", password)); //Add password param
        post.setEntity(new UrlEncodedFormEntity(params, StandardCharsets.UTF_8)); //set post entity

        CloseableHttpResponse response = client.execute(post); //Execute the post request
        HttpEntity entity = response.getEntity(); //Get the response entity

        StringBuffer sb = new StringBuffer(); //Create new string buffer

        try {
            if (entity != null) { // Check if entity exists
                InputStream in = entity.getContent(); //Create new input stream
                BufferedReader reader = new BufferedReader(new InputStreamReader(in)); //Create BufferedReader
                String line; //Create line variable
                try {
                    while((line = reader.readLine()) != null) // while there is a line read it
                        sb.append(line); //append line to buffer
                } finally {
                    in.close(); //close the reader
                }
            }
        } finally {
            response.close(); //close the response
            client.close(); //close the client
        }

        return sb.toString(); //return response
    }

    /**
     * Gets the orders for the currently logged in user
     * @return JSON array of orders for the current user.
     * @throws IOException When the connection has failed.
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getOrders() throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(URL + ":" + PORT + "/orders/getAllFromUser");

        List<NameValuePair> params = new ArrayList<>(1);
        params.add(new BasicNameValuePair("userId", User.getInstance().getUserId()+""));
        post.setEntity(new UrlEncodedFormEntity(params, StandardCharsets.UTF_8));

        CloseableHttpResponse response = client.execute(post);
        HttpEntity entity = response.getEntity();

        StringBuffer sb = new StringBuffer();

        try {
            if (entity != null) {
                InputStream in = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                try {
                    while((line = reader.readLine()) != null)
                        sb.append(line);
                } finally {
                    in.close();
                }
            }
        } finally {
            response.close();
            client.close();
        }


        String str = sb.toString();
        System.out.println(str);
        return str;
    }

}
