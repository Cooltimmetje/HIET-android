package nl.hetisetenstijd.androidapp;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import org.json.simple.parser.ParseException;

import java.io.IOException;

/**
 * Login view controller.
 * @author Tim Medema
 */
public class LoginView extends Fragment {

    private EditText emailField;
    private EditText passwordField;

    /**
     * Gets called when the view gets created.
     * @return The view
     */
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.login_screen, container, false); //Inflate the login_screen layout.
        emailField = view.findViewById(R.id.email_field); //Save the emailField to a variable.
        passwordField = view.findViewById(R.id.password_field); //Save the passwordField to a variable.
        return view; //return the view.
    }

    /**
     * Gets called when the view has been created.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState); //Call super

        view.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() { //Find the back_button and register a on click.
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LoginView.this)
                        .navigate(R.id.back_to_home); //Return to the homepage.
            }
        });

        view.findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() { //Find the login_button and register a on click
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(final View view){
                final Runnable runnable = new Runnable(){ //Create runnable, to run this on a different thread, network traffic on main thread is not allowed.
                    @Override
                    public void run() {
                        boolean loginSucceeded = false;
                        String email = emailField.getText().toString(); //Get the value from the email field.
                        String password = passwordField.getText().toString(); //Get the value from the password field.

                        if(email.equals("") || password.equals("")){ //Check if either are empty.
                            Snackbar.make(view, R.string.no_credentials, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show(); //Show snackbar saying user needs to enter their credentials
                            return; //stop
                        }

                        String token = null;
                        try {
                            token = API.getInstance().login(email, password); //Get token from API
                        } catch (IOException e){
                            e.printStackTrace();
                            Snackbar.make(view, R.string.no_server_connection, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show(); //Show error something on the connection has gone wrong, user must check their internet connection.
                        }

                        if(token != null) { //Check if token has been recieved.
                            try {
                                User.getInstance().setToken(token); //Set the token
                                loginSucceeded = true; //Set loginSucceeded to true
                            } catch (ParseException e) { //Parse exception happens when we do not get a token back, we can assume that the user has entered invalid details here.
                                e.printStackTrace();
                                Snackbar.make(view, R.string.incorrect_login_details, Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show(); //Show error snackbar.
                            }
                        }

                        if(loginSucceeded)
                            NavHostFragment.findNavController(LoginView.this).navigate(R.id.to_orders); //If login has succeeded, go to order screen.
                    }
                };

                new Thread(runnable).start(); // Create new thread from runnable and start it.
            }
        });
    }
}